#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void exit_handler() {
    printf("Выход из программы\n");
}

void sigint_handler(int signum) {
    printf("Обработчик SIGINT: Получен сигнал %d (Ctrl+C)\n", signum);
}

void sigterm_handler(int signum, siginfo_t *info, void *context) {
    printf("Обработчик SIGTERM: Получен сигнал %d (%s)\n", signum, strsignal(signum));

    if (info->si_pid > 0) {
        printf("PID отправителя: %d\n", info->si_pid);
    }
    
    exit(0);
}

int main() {
    // Устанавливаем обработчик atexit()
    atexit(exit_handler);

    // Устанавливаем обработчик SIGINT с использованием signal()
    signal(SIGINT, sigint_handler);

    // Устанавливаем обработчик SIGTERM с использованием sigaction()
    struct sigaction sa;
    sa.sa_sigaction = sigterm_handler;
    sa.sa_flags = SA_SIGINFO;
    sigaction(SIGTERM, &sa, NULL);

    pid_t child_pid = fork();

    if (child_pid < 0) {
        perror("Ошибка при вызове fork()");
        return 1;
    } else if (child_pid == 0) {
        // Код, выполняемый в дочернем процессе
        printf("Дочерний процесс, PID: %d\n", getpid());
        sleep(10); // Пусть дочерний процесс работает 10 секунд
    } else {
        // Код, выполняемый в родительском процессе
        printf("Родительский процесс, PID: %d\n", getpid());
        printf("Ожидание завершения дочернего процесса...\n");
        waitpid(child_pid, NULL, 0); // Ожидание завершения дочернего процесса
    }

    return 0;
}
