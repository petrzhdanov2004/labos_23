#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    
    if (execvp("ls", argv) == -1) {
        perror("execvp");
        exit(EXIT_FAILURE);
    }

    return 0;
}