#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include <string.h>

#include "message.h"

int main() {

    key_t key = ftok("shared_memory", 65); // Генерация ключа

    int shmid = shmget(key, sizeof(msg_t), 0666); // Создание сегмента разделяемой памяти

    msg_t *mes = (msg_t*) shmat(shmid, NULL, 0);

    while (1) {
        sleep(1);

        time_t nowTime;
        time(&nowTime);

        printf("Значение в разделяемой памяти [получатель]:\n");
        printf("    PID: %d\n", mes->pid);
        printf("    time: %s", ctime(&mes->time));
        printf("    text: %s\n", mes->text);
        printf("Текущее время:\n");
        printf("    time: %s\n", ctime(&nowTime));
    }

    return 0;
}
