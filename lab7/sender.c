#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/file.h>

#include <string.h>

#include "message.h"

int main() {

	key_t key = ftok("shared-memory", 65); // Генерация ключа

	int shmid = shmget(key, sizeof(msg_t), 0666 | IPC_CREAT); // Создание сегмента разделяемой памяти
	struct shmid_ds shmid_ds_info;

	// Получение информации о сегменте
	shmctl(shmid, IPC_STAT, &shmid_ds_info);

	// Вывод количества текущих подключений к сегменту
	if  ( shmid_ds_info.shm_nattch == 1 ) {
		printf("Уже запущено\n");
		exit(1);
	}

	msg_t *mes = (msg_t*) shmat(shmid, NULL, 0);

	while (1) {
		sleep(1);

		time(&mes->time);
		mes->pid = getpid();
		strcpy(mes->text, "Hello World");

		printf("Значение в разделяемой памяти [отправитель]:\n");
		printf("	PID: %d\n", mes->pid);
		printf("	time: %s", ctime(&mes->time));
		printf("	text: %s\n", mes->text);
	}

	return 0;
}