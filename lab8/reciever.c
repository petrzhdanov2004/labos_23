#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <time.h>
#include <unistd.h>

#include "message.h"

static struct sembuf sem_look = {0, -1, 0};
static struct sembuf sem_open = {0, 1, 0};

int main() {
    key_t key = ftok("shared-memory", 65);
    if (key < 0) {
        printf("error\n");
        return 1;
    }

    int shmid = shmget(key, sizeof(msg_t), 0666 | IPC_CREAT);
    if (shmid < 0) {
        printf("error\n");
        return 1;
    }

    msg_t *mes = (msg_t*) shmat(shmid, NULL, 0);
    if (mes < 0) {
        printf("Сегмент не подсоединился\n");
        return 1;
    }

    int sem = semget(key, 1, IPC_CREAT | 0666);
    if (sem < 0) {
        printf("Семафор не создался\n");
        return 1;
    }

    semop(sem, &sem_look ,1);

    time_t nowTime;
    time(&nowTime);

    printf("Значение в разделяемой памяти [получатель]:\n");
    printf("    PID: %d\n", mes->pid);
    printf("    time: %s", ctime(&mes->time));
    printf("    text: %s\n", mes->text);
    printf("Текущее время:\n");
    printf("    time: %s\n", ctime(&nowTime));

    semop(sem, &sem_open, 1);
    shmdt(mes);

    return 0;
}
