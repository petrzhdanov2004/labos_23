#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#include "message.h"

int main() {
	static struct sembuf sem_look = {0, -1, 0};
	static struct sembuf sem_open = {0, 1, 0};

	key_t key = ftok("shared-memory", 65);
    if (key < 0) {
        printf("error\n");
        return 1;
    }

    int shmid = shmget(key, sizeof(msg_t), 0666 | IPC_CREAT);
    if (shmid < 0) {
        printf("error\n");
        return 1;
    }

    struct shmid_ds options;
    shmctl(shmid, IPC_STAT, &options); 
    if (options.shm_nattch >= 1) {
        printf("Процесс существует\n");
        return 1;
    }

    msg_t *mes = (msg_t*) shmat(shmid, NULL, 0);
    if (mes < 0) {
        printf("Сегмент не подсоединился\n");
        return 1;
    }

    int sem = semget(key, 1, IPC_CREAT | 0666);
    if (sem < 0) {
        printf("Семафор не создался\n");
        return 1;
    }

    semop(sem, &sem_open, 1);

    while (1) {
		semop(sem, &sem_look ,1);

		sleep(2);

		time(&mes->time);
		mes->pid = getpid();
		strcpy(mes->text, "Hello World");

		printf("Значение в разделяемой памяти [отправитель]:\n");
		printf("	PID: %d\n", mes->pid);
		printf("	time: %s", ctime(&mes->time));
		printf("	text: %s\n", mes->text);

		semop(sem, &sem_open, 1);
    }

    shmdt(mes);
    shmctl(sem, 0, IPC_RMID);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}