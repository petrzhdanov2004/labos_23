#ifndef MESSAGE_H
#define MESSAGE_H

#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#define SHM_ID 2003

typedef struct {
	pid_t pid;
	time_t time;
	char text[11];
} msg_t;

#endif /* MESSAGE_H */