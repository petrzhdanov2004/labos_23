#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <stdbool.h>

typedef struct {
	struct stat fileStat;
	char fileName[20];
	time_t time;
} fileInfo;

int createArchive(char *filename) {

	int archive;

	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	
	archive = open(filename, O_WRONLY);

	if (archive == -1) {
		archive = creat(filename, mode);
	}

	close(archive);

	return 0;

}

int readArchive(char *archname) {

	int archive;
	
	archive = open(archname, O_RDONLY);

	if (archive == -1) {
		return 1;
	}

	size_t count = 0;

	while (1) {
		char *buf = malloc(sizeof(fileInfo));
		if (!read(archive, buf, sizeof(fileInfo))) break;
		fileInfo *a = (fileInfo*)(buf);

		printf("Имя файла: %s\n", a->fileName);
		printf("Вес файла: %ld\n", a->fileStat.st_size);
		printf("Время добавления в архив: %s\n", ctime(&a->time));

		lseek(archive, sizeof(fileInfo), SEEK_CUR);

		char bufFile[a->fileStat.st_size];
		read(archive, bufFile, a->fileStat.st_size);

		free(buf);

		count++;
	}

	printf("Всего файлов в архиве: %ld\n", count);

	close(archive);

	return 0;
}

int addFile(char *archname, char *filename) {

	if (strcmp(archname, filename) == 0) {
		printf("Архив не может архивировать сам себя\n");
		return 1;
	}

	int archive = open(archname, O_RDONLY);

	if (archive == -1) {
		return 1;
	}

	int file = open(filename, O_RDONLY);

	if (file == -1) {
		return 1;
	}

	bool fileExist = 0;

	while (1) {
		char *buf = malloc(sizeof(fileInfo));
		if (!read(archive, buf, sizeof(fileInfo))) break;
		fileInfo *a = (fileInfo*)(buf);

		if (strcmp(a->fileName, filename) == 0) {
			fileExist = 1;
		}

		lseek(archive, sizeof(fileInfo), SEEK_CUR);

		char bufFile[a->fileStat.st_size];
		read(archive, bufFile, a->fileStat.st_size);

		free(buf);
	}

	if (fileExist) {
		close(archive);
		printf("Файл %s уже находится в архиве\n", filename);
		return 1;	
	}
	
	archive = open(archname, O_WRONLY);

	fileInfo fileinf;
	struct stat file_info;
	if (stat(filename, &file_info) != 0) {
		return 1;
	}

	time_t now;
    time(&now);

	strcpy(fileinf.fileName, filename);
	fileinf.fileStat = file_info;
	fileinf.time = now;

	lseek(archive, 0, SEEK_END);

	write(archive, &fileinf, sizeof(fileInfo));

	lseek(archive, sizeof(fileInfo), SEEK_CUR);


	char bufFile[file_info.st_size];
	read(file, bufFile, file_info.st_size);

	write(archive, &bufFile, file_info.st_size);

	close(archive);

	close(file);

	return 0;
}

int deleteFile(char *archname, char *filename) {
	
	createArchive(".bufArch");

	int bufArch = open(".bufArch", O_WRONLY);

	int archive = open(archname, O_RDONLY);

	bool fileExist = 1;

	if (archive == -1) {
		return 1;
	}

	while (1) {
		char *buf = malloc(sizeof(fileInfo));
		if (!read(archive, buf, sizeof(fileInfo))) break;
		fileInfo *a = (fileInfo*)(buf);

		lseek(archive, sizeof(fileInfo), SEEK_CUR);

		char bufFile[a->fileStat.st_size];
		read(archive, bufFile, a->fileStat.st_size);

		if (strcmp(a->fileName, filename)) {

			write(bufArch, a, sizeof(fileInfo));
			lseek(bufArch, sizeof(fileInfo), SEEK_CUR);
			write(bufArch, &bufFile, a->fileStat.st_size);
		} else {
			fileExist = 0;
		}

		free(buf);
	}

	close(archive);
	close(bufArch);

	remove(archname);
	rename(".bufArch", archname);

	return fileExist;

}

int extractFile(char *archname, char *filename) {
	createArchive(".bufArch");

	int bufArch = open(".bufArch", O_WRONLY);

	int archive = open(archname, O_RDONLY);

	if (archive == -1) {
		return 1;
	}

	bool fileExist = 1;

	while (1) {
		char *buf = malloc(sizeof(fileInfo));
		if (!read(archive, buf, sizeof(fileInfo))) break;
		fileInfo *a = (fileInfo*)(buf);

		lseek(archive, sizeof(fileInfo), SEEK_CUR);

		char bufFile[a->fileStat.st_size];
		read(archive, bufFile, a->fileStat.st_size);

		if (strcmp(a->fileName, filename)) {

			write(bufArch, a, sizeof(fileInfo));
			lseek(bufArch, sizeof(fileInfo), SEEK_CUR);
			write(bufArch, &bufFile, a->fileStat.st_size);
		} else {
			mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

			char flag;

			if (access(filename, F_OK) != -1) {
			    printf("Файл %s будет перезаписан. Вы хотите продолжить [y/n]?\n", filename);
			    scanf("%c", &flag);
			    if (flag == 'n') {
			    	return 2;
			    }
			}
    

			int file = creat(filename, mode);
			write(file, &bufFile, a->fileStat.st_size);
			close(file);
			fileExist = 0;

			chmod(filename, a->fileStat.st_mode);

		}

		free(buf);
	}

	close(archive);
	close(bufArch);

	remove(archname);
	rename(".bufArch", archname);


	return fileExist;
}

int main(int argc, char* argv[]) {

	bool cFlag = 0; // Создать архив
	bool dFlag = 0; // Удалить файл
	bool eFlag = 0;	// Извлечь файл
	bool iFlag = 0; // Добавить файл
	bool hFlag = 0; // Вывод помощи
	bool rFlag = 0; // Вывод информации

	int c;

	if (argc <= 1) hFlag = 1;

	while ((c = getopt(argc, argv, "cdeihr")) != -1) {
		switch (c) {
			case 'c':
				cFlag = 1;
				break;
			case 'd':
				dFlag = 1;
				break;
			case 'e':
				eFlag = 1;
				break;
			case 'i':
				iFlag = 1;
				break;
			case 'h':
				hFlag = 1;
				break;
			case 'r':
				rFlag = 1;
				break;
			default:
				hFlag = 1;
		}
	}

	if (hFlag) {
		printf("Примитивный архиватор\n");
		printf("-c создать архив\n");
		printf("	Синтаксис:\n");
		printf("		./archiver -c [archive name №1] [archive name №2] ...\n");
		printf("	Пример:\n");
		printf("		./archiver -c archive\n\n");
		printf("-d удалить файл из архива\n");
		printf("	Синтаксис:\n");
		printf("		./archiver -d [archive name] [file name №1] [file name №2] ...\n");
		printf("	Пример:\n");
		printf("		./archiver -d archive file\n\n");
		printf("-e извлечь файл из аhхива\n");
		printf("	Синтаксис:\n");
		printf("		./archiver -e [archive name] [file name №1] [file name №2] ...\n");
		printf("	Пример:\n");
		printf("		./archiver -e archive file\n\n");
		printf("-i добавить файл в архив\n");
		printf("	Синтаксис:\n");
		printf("		./archiver -i [archive name] [file name №1] [file name №2] ...\n");
		printf("	Пример:\n");
		printf("		./archiver -i archive file\n\n");
		printf("-r Вывести информацию об архиве\n");
		printf("	Синтаксис:\n");
		printf("		./archiver -r [archive name]\n");
		printf("	Пример:\n");
		printf("		./archiver -r archive\n\n");
		printf("-h вывести информацию об утилите\n");
	}

	if (cFlag) {
		for ( ; optind < argc; optind++) {
			if (!createArchive(argv[optind])) printf("Архив %s создан\n", argv[optind]);
			else printf("Архив %s не создан. Произошла ошиба\n", argv[optind]);
		}
	}

	if (iFlag) {

		char *archive = malloc(sizeof(argv[optind]));
		strcpy(archive, argv[optind]);
		optind++;

		for ( ; optind < argc; optind++) {
			if (!addFile(archive, argv[optind])) printf("Файл %s добавлен в архив %s успешно\n", argv[optind], archive); 
			else printf("Произошла ошибка добавления файла\n");
		}
	}

	if (dFlag) {

		char *archive = malloc(sizeof(argv[optind]));
		strcpy(archive, argv[optind]);
		optind++;

		for ( ; optind < argc; optind++) {
			if (!deleteFile(archive, argv[optind])) printf("Файл %s удалён из архива  %s успешно\n", argv[optind], archive); 
			else printf("Произошла ошибка удаления файла\n");
		}
	}

	if (eFlag) {

		char *archive = malloc(sizeof(argv[optind]));
		strcpy(archive, argv[optind]);
		optind++;

		for ( ; optind < argc; optind++) {
			size_t state = extractFile(archive, argv[optind]);
			if (!state) printf("Файл %s извлечён из архива  %s успешно\n", argv[optind], archive); 
			if (state == 1) printf("Произошла ошибка извлечения файла\n");
			if (state == 2) printf("Файл не будет извлечён\n");
		}
	}

	if (rFlag) {

		if (readArchive(argv[optind])) printf("Архива %s не существует\n", argv[optind]);
		
	}

	return 0;
}