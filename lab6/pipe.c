#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>

typedef struct {
	pid_t pid;
	char text[12];
	time_t time;
} message;

/*
~ Дескриптор 0 - на чтение
~ Дескриптор 1 - на запись
*/

int main() {

	int pipe_fd[2];
	int pid;

	if (pipe(pipe_fd)) {
		printf("Pipe error\n");
		return 1;
	}

	pid = fork();

	if (pid == -1) {
		printf("fork() failed\n");

		close(pipe_fd[0]);
		close(pipe_fd[1]);

		return 1;
	}

	if (pid == 0) { /*child pid*/
		message* cMessage = malloc(sizeof(message));

		close(pipe_fd[1]);

		read(pipe_fd[0], cMessage, sizeof(message));

		time_t currentTime = 0;
		time(&currentTime);

		printf("Получаемое сообщение (дочерний процесс): %d %s %s", cMessage->pid, cMessage->text, ctime(&cMessage->time));
		printf("Текущее время: %s", ctime(&currentTime));

		close(pipe_fd[0]);

		free(cMessage);

		return 0;

	} else { /*parent pid*/
		message* pMessage = malloc(sizeof(message)); /*Это будет отправляться*/
		pMessage->pid = getpid();
		strcpy(pMessage->text, "Hello World");
		time(&pMessage->time);

		printf("Отправляемое сообщение (родительский процесс): %d %s %s", pMessage->pid, pMessage->text, ctime(&pMessage->time));

		sleep(5);

		close(pipe_fd[0]);

		if (write(pipe_fd[1], pMessage, sizeof(message)) == -1) {
			printf("Write error\n");
			return 1;
		}

		free(pMessage);

		close(pipe_fd[1]);
		
	}

	return 0;
}

