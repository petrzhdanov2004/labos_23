#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define READ_THREADS_NUM 10

typedef struct {
	int array[100];
	int count;
} thread_array;

pthread_mutex_t mutex;

void* write_thread(void* thread_data) {

	while (1) {
		pthread_mutex_lock(&mutex);

		thread_array* shared_array = (thread_array*) thread_data;
		shared_array->count += 1;
		shared_array->array[shared_array->count] = shared_array->count;
		
		sleep(1);
		pthread_mutex_unlock(&mutex);
		usleep(100);
	}

	pthread_exit(0);
}

void* read_thread(void* thread_data){

	while (1) {

		pthread_mutex_lock(&mutex);

		thread_array* shared_array = (thread_array*) thread_data;
		printf("array[%d] = %d. tid: %lx\n", shared_array->array[shared_array->count], shared_array->array[shared_array->count], pthread_self());
		pthread_mutex_unlock(&mutex);

		usleep(100);
	}
	//завершаем поток
	pthread_exit(0);
}

int main() {

	thread_array shared_array;
	shared_array.count = 0;

	pthread_mutex_init(&mutex, NULL);

	pthread_t wariting_array;
	pthread_t reading_threads[READ_THREADS_NUM];

	pthread_create(&wariting_array, NULL, write_thread, (void *)&shared_array);
	for (int i = 0; i < READ_THREADS_NUM; i++) {
		pthread_create(&reading_threads[i], NULL, read_thread, (void *)&shared_array);
	}

	pthread_join(wariting_array, NULL);
	for (int i = 0; i < READ_THREADS_NUM; i++) {
		pthread_join(reading_threads[i], NULL);
	}

	pthread_mutex_destroy(&mutex);

	return 0;
}
